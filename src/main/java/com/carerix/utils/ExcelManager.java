package com.carerix.utils;

import lombok.SneakyThrows;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

import static com.carerix.utils.StreamUtils.asStream;
import static java.util.Optional.ofNullable;

public class ExcelManager {
	private static Workbook wb = new XSSFWorkbook();

	public static void write(List<Integer> integers) {
		Sheet sheet = createSheet();

		int ceil = (int) Math.ceil(integers.size() / 10);
		for (int i = 0; i < ceil; i++) {
			Row row = sheet.createRow(i);
			for (int j = 0; j < 10; j++) {
				fillCell(row, j, integers.get(i * 10 + j));
			}
		}
		writeContentToFile();

	}

	public static void updateTableState(List<Integer> scenarioNumbers) {
		Sheet sheet = getSheet();
		asStream(sheet.rowIterator(), false)
				.forEach(row ->
						asStream(row.cellIterator(), false)
								.forEach(cell -> {
									int numericCellValue = (int) cell.getNumericCellValue();
									if (scenarioNumbers.contains(numericCellValue))
										cell.setCellStyle(getCellStyle());
								}));
		writeContentToFile();
	}


	@SneakyThrows
	private static void writeContentToFile() {
		FileOutputStream fileOut = new FileOutputStream("poi-generated-file.xlsx");
		wb.write(fileOut);
		fileOut.close();
		wb.close();
	}

	private static CellStyle getCellStyle() {
		CellStyle cellStyle = wb.createCellStyle();
		Font font = wb.createFont();
		font.setBold(true);
		cellStyle.setFont(font);
		cellStyle.setFillForegroundColor(HSSFColor.HSSFColorPredefined.BRIGHT_GREEN.getIndex());
		cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		return cellStyle;
	}

	private static void fillCell(Row row, Integer cellNumber, Integer integer) {
		row.createCell(cellNumber).setCellValue(integer);
	}

	private static Sheet createSheet() {
		wb = new XSSFWorkbook();
		return ofNullable(wb.getSheet("Sheet0"))
				.orElse(wb.createSheet());
	}

	@SneakyThrows
	private static Sheet getSheet() {
		FileInputStream fis = new FileInputStream("poi-generated-file.xlsx");
		wb = new XSSFWorkbook(fis);
		return ofNullable(wb.getSheet("Sheet0")) .orElse(wb.createSheet());
	}

}
