package com.carerix.utils;


import lombok.SneakyThrows;

import java.util.Properties;

public final class PropertiesUtils {
	private static final String ENVIRONMENT = "env";

	/**
	 * Utility class does not require creation of instance
	 */
	private PropertiesUtils() {
	}

	/**
	 * Method reads properties
	 *
	 * @param propertyName
	 * @return
	 */
	public static String getProperty(String propertyName) {
		return readPropFile().getProperty(propertyName);
	}

	@SneakyThrows
	private static Properties readPropFile() {
		String environment = System.getProperty(ENVIRONMENT);
		Properties prop = new Properties();
		prop.load(PropertiesUtils.class.getClassLoader().getResourceAsStream(environment));
		return prop;
	}
}
