package com.carerix.utils;

import lombok.SneakyThrows;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
	private static final Path SCENARIO_ROOT_PATH = Paths.get(PropertiesUtils.getProperty("scenario_root_path"));
	private static List<String> CONTENT_BY_PATH = new ArrayList<>();

	@SneakyThrows
	public static List<String> getLinesContentByPath() {
		Files.walk(SCENARIO_ROOT_PATH)
				.filter(Files::isRegularFile)
				.forEach(FileUtils::getContentByFile);
		return CONTENT_BY_PATH;
	}

	@SneakyThrows
	private static void getContentByFile(Path f) {
		Files.lines(f).forEach(CONTENT_BY_PATH::add);
	}
}
