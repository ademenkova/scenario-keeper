package com.carerix;

import com.carerix.core.ScenarioManager;
import com.carerix.utils.ExcelManager;

import java.util.List;

import static com.carerix.core.NumberGenerator.generateInRange;
import static com.carerix.core.ScenarioManager.getActualScenarioNumbers;

public class ScenarioKeeper {
	public static void main(String[] args) {
		List<Integer> integers = generateInRange(1, 2001);
		ExcelManager.write(integers);
		List<Integer> actualScenarioNumbers = getActualScenarioNumbers();
		ExcelManager.updateTableState(actualScenarioNumbers);
		integers.removeAll(actualScenarioNumbers);
		ScenarioManager.updateFiles(integers);
	}
}
