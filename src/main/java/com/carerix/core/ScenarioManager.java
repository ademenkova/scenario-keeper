package com.carerix.core;

import com.carerix.utils.FileUtils;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;

import static com.carerix.utils.FileUtils.getLinesContentByPath;
import static com.carerix.utils.PropertiesUtils.getProperty;
import static java.util.stream.Collectors.toList;

public class ScenarioManager {
	private static final Path SCENARIO_ROOT_PATH = Paths.get(getProperty("scenario_root_path"));
	private static final Pattern pattern = Pattern.compile("(^Scenario: ([^0-9]+).*)|(^Scenario:$)");

	public static List<Integer> getActualScenarioNumbers() {
		List<String> filesContentByPath = getLinesContentByPath();
		List<Integer> scenarioNumbers = findScenarioNumbers(filesContentByPath);
		scenarioNumbers.sort(Comparator.naturalOrder());
		return scenarioNumbers;
	}

	@SneakyThrows
	public static void updateFiles(List<Integer> freeScenarioNumbers) {
		Files.walk(SCENARIO_ROOT_PATH)
				.filter(Files::isRegularFile)
				.forEach(a -> mod(a, freeScenarioNumbers));
	}


	@SneakyThrows
	private static void mod(Path file, List<Integer> freeScenarioNumbers) {
		AtomicBoolean changed = new AtomicBoolean(false);
		List<String> linesByFile = Files.lines(file)
				.map(line -> {
							if (pattern.matcher(line).matches()) {
								line = modifyScenarioLine(freeScenarioNumbers, line);
								changed.set(true);
							}
							return line;
						}
				)
				.collect(toList());
		if (changed.get()) {
			Files.write(file, linesByFile);
		}
	}

	private static String modifyScenarioLine(List<Integer> freeScenarioNumbers, String line) {
		Integer first = freeScenarioNumbers.get(0);
		freeScenarioNumbers.remove(0);
		line = line.replace(":", ": " + first);
		return line;
	}


	private static List<Integer> findScenarioNumbers(List<String> content) {
		return content.stream()
				.filter(line -> line.contains("Scenario"))
				.map(line -> StringUtils.substringAfter(line, "Scenario: "))
				.map(line -> line.replaceAll("\\D+(?<!^)\\d{0,}", ""))
				.filter(StringUtils::isNotEmpty)
				.map(Integer::parseInt)
				.collect(toList());
	}
}
