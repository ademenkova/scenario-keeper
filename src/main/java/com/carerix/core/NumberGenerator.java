package com.carerix.core;

import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

public class NumberGenerator {
	public static List<Integer> generateInRange(int i, int i1) {
		return  IntStream.range(i, i1)
				.boxed()
				.collect(toList());
	}
}
